# # encoding: utf-8

# Inspec test for recipe tomcat::server

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/


describe package('java-1.7.0-openjdk-devel') do
  it { should be_installed }
end

describe group('tomcat') do
  it { should exist }
end

describe user('tomcat') do
  it { should exist }
  its('group') { should eq 'tomcat' }
  its('home') { should eq '/opt/tomcat' }
  its('shell') { should eq '/bin/nologin' }
end

describe file '/tmp/apache-tomcat-8.5.40.tar.gz' do
  it { should exist }
end

describe file('/opt/tomcat') do
  it { should exist}
  it { should be_directory }
end


# Check if some of the Tomcat files were uncompressed into Tomcat home

['BUILDING.txt', 'CONTRIBUTING.md', 'LICENSE', 'NOTICE'].each do |tomcatfile| 
  describe file "/opt/tomcat/#{tomcatfile}" do
    it { should exist}
  end
end 

describe file '/opt/tomcat/conf' do
  its('group') { should eq 'tomcat' }
end


describe file('/etc/systemd/system/tomcat.service') do
  it { should exist}
end

describe service('tomcat') do
  it { should be_running }
  it { should be_enabled }
end

  

