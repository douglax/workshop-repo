#
# Cookbook:: build_cookbook
# Recipe:: provision
#
# Copyright:: 2019, Alejandro Acosta, All Rights Reserved.
include_recipe 'delivery-truck::provision'
