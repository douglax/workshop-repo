#
# Cookbook:: build_cookbook
# Recipe:: lint
#
# Copyright:: 2019, Alejandro Acosta, All Rights Reserved.
include_recipe 'delivery-truck::lint'
