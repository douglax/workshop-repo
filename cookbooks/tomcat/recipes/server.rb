#
# Cookbook:: tomcat
# Recipe:: server
#
# Copyright:: 2019, Alejandro Acosta, All Rights Reserved.

package 'java-1.7.0-openjdk-devel' do
  action :install
end

group 'tomcat' do
  action :create
end

user 'tomcat' do
  comment 'Tomcat service account'
  group 'tomcat'
  home '/opt/tomcat'
  shell '/bin/nologin'
  action :create
end

remote_file '/tmp/apache-tomcat-8.5.40.tar.gz' do
  source 'https://www-eu.apache.org/dist/tomcat/tomcat-8/v8.5.40/bin/apache-tomcat-8.5.40.tar.gz'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

directory '/opt/tomcat' do
    owner 'tomcat'
    group 'tomcat'
    mode '0755'
    action :create
end

execute 'untar tomcat' do
    command 'tar xvf /tmp/apache-tomcat-8.5.40.tar.gz -C /opt/tomcat --strip-components=1'
    action :run
end

directory '/opt/tomcat/conf' do
    owner 'root'
    group 'tomcat'
    mode '0775'
    action :create
end

execute 'chmod to conf' do
    command 'chmod g+r /opt/tomcat/conf/*'
    action :run
end


['webapps', 'work', 'temp', 'logs'].each do |tomcatdir| 
    execute "chown tomcat dir #{tomcatdir}" do
        command "chown -R tomcat /opt/tomcat/#{tomcatdir}/"
        action :run
    end
end 


template '/etc/systemd/system/tomcat.service' do
    source 'tomcat.service.erb'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

execute 'reload systemd daemon' do
    command 'systemctl daemon-reload'
    action :run
end

service 'tomcat' do
    action [:start, :enable]
end


